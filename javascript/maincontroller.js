'use strict'
const $ = function (bar) { return document.getElementById(bar); };
function createHeader() {
    let head = $('navigationHeader');
    head.setAttribute("class", "navigationBar");
    let l1 = document.createElement("li");
    let a1 = document.createElement("a");
    let l2 = document.createElement("li");
    let a2 = document.createElement("a");


    l1.setAttribute("class", "navigationList");
    a1.setAttribute("class", "navigationText");
    a1.setAttribute("href", "index.html");
    a1.innerHTML = "Forside";
    l1.appendChild(a1);


    l2.setAttribute("class", "navigationList");
    a2.setAttribute("class", "navigationText");
    a2.setAttribute("href", "Conway´s game.html");
    a2.innerHTML = "Conway´s game";
    l2.appendChild(a2);

    head.appendChild(l1);
    head.appendChild(l2);

}

const init = function () {
  /**call functions */
}
window.addEventListener("load", init);
